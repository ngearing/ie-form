<?php

namespace IE_Form;

use GFAPI;
use GFFormDisplay;

if ( class_exists( 'IE_Form' ) ) {
	return;
}

class IE_Form {

	var $title = 'The Adolescent Swinburne University Emotional Intelligence Test';
	var $id    = null;
	var $form  = null;

	public function __construct( $id = false ) {

		if ( ! class_exists( 'GFAPI' ) ) {
			return false;
		}

		if ( $id ) {
			$form       = GFAPI::get_form( $id );
			$this->form = $form;
			$this->id   = $form['id'];
		} else {
			$this->get_form();
		}

		if ( ! isset( $this->form ) ) {
			return false;
		}
	}

	/**
	 * Check if the user has already submitted an entry.
	 *
	 * @param [type] $lead_id
	 * @param [type] $form
	 * @return void
	 */
	public function user_entry_check( $lead_id, $form ) {
		$entry = GFAPI::get_entries(
			$form['id'],
			[
				'field_filters' => [
					[
						'key'   => 'created_by',
						'value' => get_current_user_id(),
					],
				],
			]
		);

		if ( $entry && ! is_wp_error( $entry ) ) {
			return array_pop( $entry )['id'];
		}

		return false;
	}

	public function calculate_ie( $entry, $form ) {
		$fields    = $form['fields'];
		$ie_values = [];

		foreach ( $fields as $key => $field ) {
			if ( ! $field->inputName ) {
				continue;
			}

			// Get results field to update the value.
			if ( $field->inputName == 'results' ) {
				$results_field = $field;
			}

			$ie_key = explode( '-', $field->inputName )[0];
			if ( ! isset( $ie_values[ $ie_key ] ) ) {
				$ie_values[ $ie_key ] = [
					'count' => 0,
					'value' => 0,
				];
			}

			$ie_values[ $ie_key ]['count']++;
			$ie_values[ $ie_key ]['value']  += $this->get_field_value( $field );
			$ie_values[ $ie_key ]['percent'] = $ie_values[ $ie_key ]['value'] / ( $ie_values[ $ie_key ]['count'] * 5 ) * 100;
		}

		$ie_entry = new IE_Entry( $entry['id'] );
		$ie_entry->set_score( $ie_values, $this->id );
		$file = $ie_entry->create_pdf();
		$url  = ( new IE() )->path_to_url( $file );

		$ie_entry->update_meta( $results_field->id, $url ); // Update meta as entry has already been saved.
		$entry[ $results_field->id ] = $url; // Set entry value for submission notifications.

		return $entry;
	}

	public function set_teacher_email( $notification, $form, $lead ) {

		if ( $notification['to'] == '{user:[teacher]}' ) {
			$user_id = get_current_user_id();
			$teacher = get_user_meta( $user_id, 'teacher', true );
			$teacher = get_user_by( 'id', $teacher );
			$subject = $notification['subject'];

			if ( ! $teacher ) {
				$email   = get_option( 'admin_email' );
				$subject = "** Teacher not found ** $subject";
			} else {
				$email = $teacher->user_email;
			}

			$notification['to']      = $email;
			$notification['subject'] = $subject;
		}

		return $notification;
	}

	public function choice_markup( $content, $field, $value, $lead_id, $form_id ) {
		if ( $form_id !== $this->id ) {
			return $content;
		}

		if ( $field->type == 'radio' ) {
			$label       = $field->label;
			$question_no = $this->question_no ?: 1;
			$new_label   = "<span class='question-no'>Question $question_no</span>$label";

			$content = str_replace( $label, $new_label, $content );
			$question_no++;
			$this->question_no = $question_no;
		}

		return $content;
	}

	public function get_field_value( $field ) {
		return $field->get_input_value_submission( 'input_' . $field->id );
	}

	public function add_form_sections_class( $form ) {

		$section = $this->get_form_page_section( $form );

		$form['cssClass'] = "section-$section";

		return $form;
	}

	public function add_body_form_section( $class ) {
		global $post;
		if ( ! $post || ! is_page() ) {
			return $class;
		}

		preg_match( '/\[gravityform id\=[\"\'](\d*)[\"\'].+\]/', $post->post_content, $matches );
		if ( isset( $matches[1] ) ) {
			$form_id = $matches[1];
			$section = $this->get_form_page_section( GFAPI::get_form( $form_id ) );
			$class[] = "form-section-$section";
		}

		return $class;
	}

	public function get_form_page_section( $form ) {
		$page    = GFFormDisplay::get_current_page( $form['id'] );
		$last    = GFFormDisplay::get_max_page_number( $form );
		$section = ceil( ( $page / $last * 100 ) / 25 );

		return $section;
	}

	public function update_confirmation( $message, $form, $entry ) {

		$entry = new IE_Entry( $entry['id'] );

		$new_content = sprintf(
			'<p>You can view your score <a href="%s">here</a>.</p>',
			$entry->get_results_url()
		);

		return $message . $new_content;
	}

	public static function create() {
		if ( ( new self() )->get_form() ) {
			return ( new self() )->id;
		}

		$file   = __DIR__ . '/questions.json';
		$fields = file_get_contents( $file );
		$fields = json_decode( $fields, true );

		$form = GFAPI::add_form(
			[
				'title'                => ( new self() )->title,
				'is_active'            => '1',
				'date_created'         => date( 'Y-m-d H:m:s' ),
				'description'          => '',
				'labelPlacement'       => 'top_label',
				'descriptionPlacement' => 'below',
				'fields'               => $fields,
				'button'               => [
					'type'     => 'text',
					'text'     => 'Submit',
					'imageUrl' => '',
				],
			]
		);

		return $form;
	}

	public function get_form() {
		if ( ! isset( $this->id ) || ! isset( $this->form ) ) {
			$forms       = GFAPI::get_forms();
			$form_titles = array_column( $forms, 'title' );
			$f_key       = array_search( $this->title, $form_titles );
			if ( $f_key ) {
				// Form exists.
				$form       = $forms[ $f_key ];
				$this->form = $form;
				$this->id   = $form['id'];

				return $form;
			} else {
				return false;
			}
		}

		return $this->form;
	}
}
