<?php

namespace Ngearing\Wp;

class Uploads {


	public function __construct() {
		$uploads = wp_upload_dir();
		foreach ( $uploads as $key => $value ) {
			$this->{ $key } = $value;
		}
	}

	/**
	 * Create folder if it does not exist.
	 *
	 * @param string $file
	 * @param string $mode
	 * @param bool   $recursive
	 * @return void
	 */
	public function create( $file, $mode = '0755', $recursive = true ) {
		$base = isset( $this->new_basedir ) ? $this->new_basedir : $this->basedir;

		if ( file_exists( "$base/$file" ) ) {
			return;
		}

		return mkdir( "$base/$file", $mode, $recursive );
	}

	public function __get( $file ) {
		return $this->get_basedir() . "/$name";
	}

	public function get_basedir() {
		return isset( $this->new_basedir ) ? $this->new_basedir : $this->basedir;
	}

	public function set_basedir( $file ) {
		$this->new_basedir = $this->basedir . "/$file";
	}

}
