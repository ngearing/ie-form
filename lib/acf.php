<?php

namespace IE_Form;

use MeprProduct;
use MeprTransaction;
use MeprUser;

class ACF {

	public function load_json( $paths ){
		$paths[] = IE_FORM_PATH . '/lib/acf-json';

		return $paths;
	}

	public function update_membership( $value, $post_id, $field ) {

		$user_id = (int) str_replace('user_', '', $post_id);
		$user = new MeprUser($user_id);

		$trans = MeprTransaction::get_all_by_user_id($user_id);
		$current = $user->active_product_subscriptions();
		$current_trans = [];

		foreach ($value as $product_id ) {
			// Create transaction with no active product.
			if ( ! in_array($product_id, $current) ) {
				$product = new MeprProduct( $product_id );

				$txn = new MeprTransaction();
				$txn->user_id = $user->ID;
				$txn->product_id = $product->ID;

				$price = $product->adjusted_price();
				$txn->set_subtotal($price);
				$txn->gateway = MeprTransaction::$free_gateway_str;

				$txn->status = 'complete';
				$txn->store();
				$current_trans[] = $txn->id;
			} else {
				$txns = array_filter( $trans, function ($txn) use ( $product_id ) {
					return $txn->product_id == $product_id;
				});
				if ( $txns ) {
					$current_trans[] = array_pop($txns)->id;
				}
			}
		}

		foreach ($trans as $txn) {
			if ( ! in_array( $txn->id, $current_trans ) ) {
				$txn = new MeprTransaction($txn->id);
				$txn->destroy();
			}
		}

		return $value;
	}
}
