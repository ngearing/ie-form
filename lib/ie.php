<?php

namespace IE_Form;

if ( class_exists( 'IE' ) ) {
	return;
}

class IE {

	var $name = 'ie-form';

	public function __construct() {
		$this->debug = true;

		$this->load_dependencies();
		$this->load_scripts();

		$this->register_acf_hooks();

		$this->register_gform_hooks();
		if ( $this->debug ) {
			$this->register_gform_dev_hooks();
		}

		$this->register_admin_hooks();
		$this->add_template_redirection();
	}

	private function load_dependencies() {
		require IE_FORM_PATH . '/vendor/autoload.php';

		$this->loader = new \Ngearing\Wp\Loader();
		$this->form   = new IE_Form();
		if ( $this->debug ) {
			$this->form = new IE_Form_Dev();
		}
	}

	private function register_acf_hooks() {
		$acf = new ACF();
		$this->loader->add_filter( 'acf/settings/load_json', $acf, 'load_json', 10, 1 );
		$this->loader->add_filter( 'acf/update_value/name=membership', $acf, 'update_membership', 10, 3 );
	}

	private function register_gform_hooks() {
		$form_id = $this->form->id;

		$this->loader->add_filter( 'body_class', $this->form, 'add_body_form_section', 10, 1 );

		$this->loader->add_filter( "gform_entry_post_save_$form_id", $this->form, 'calculate_ie', 10, 2 );
		$this->loader->add_filter( "gform_confirmation_$form_id", $this->form, 'update_confirmation', 10, 3 );
		$this->loader->add_filter( "gform_pre_render_$form_id", $this->form, 'add_form_sections_class', 10, 1 );

		$this->loader->add_filter( "gform_entry_id_pre_save_lead_$form_id", $this->form, 'user_entry_check', 10, 2 );
		$this->loader->add_filter( "gform_notification_$form_id", $this->form, 'set_teacher_email', 10, 3 );
	}

	private function register_gform_dev_hooks() {
		$form_id = $this->form->id;

		$this->loader->add_filter( 'gform_form_tag', $this->form, 'render_test_submit', 10, 2 );
		$this->loader->add_filter( "gform_pre_process_$form_id", $this->form, 'pre_pro', 10, 1 );
	}

	private function register_admin_hooks() {
		$admin = new Admin();

		$this->loader->add_action('admin_init', $admin, 'register_roles' );
		$this->loader->add_action('admin_enqueue_scripts', $admin, 'admin_scripts' );
		$this->loader->add_action('admin_head', $admin, 'admin_notices', 1 );

		$this->loader->add_action('admin_menu', $admin, 'admin_menu', 100 );
		$this->loader->add_action('admin_bar_menu', $admin, 'admin_bar_menu', 100 );

		$this->loader->add_filter('views_users', $admin, 'admin_views_users', 100, 1 );
		$this->loader->add_filter('manage_users_columns', $admin, 'manage_users_columns', 100 );
		$this->loader->add_filter('manage_users_sortable_columns', $admin, 'manage_users_sortable_columns', 100 );
		$this->loader->add_filter('manage_users_custom_column', $admin, 'manage_users_custom_column', 100, 3 );

		$this->loader->add_action('pre_get_users', $admin, 'pre_get_users' );
		$this->loader->add_filter('editable_roles', $admin, 'editable_roles', 100, 1 );
	}

	private function add_template_redirection() {
		$redirect = new Redirect();

		$this->loader->add_action('wp_login', $redirect, 'wp_login', 10, 2 );
	}

	public function load_scripts() {
		$form_id = $this->form->id;
		$this->loader->add_action( "gform_enqueue_scripts_$form_id", $this, 'form_scripts' );
	}

	public function form_scripts() {
		wp_enqueue_style( $this->name, IE_FORM_URL . '/dist/main.css', [], filemtime( IE_FORM_PATH . '/dist/main.css' ) );
		wp_enqueue_script( $this->name, IE_FORM_URL . '/dist/main.js', [], filemtime( IE_FORM_PATH . '/dist/main.js' ), true );
	}

	public function get_file( $file ) {
		$base      = IE_FORM_PATH . '/src';
		$file_path = "$base/$file";

		return $file_path;
	}

	public function get_image( $file, $url = true ) {
		$file = $this->get_file( "images/$file" );
		if ( ! $url ) {
			return $file;
		}

		return $this->path_to_url( $file );
	}

	public function path_to_url( $path ) {
		$file = str_replace(
			ABSPATH,
			home_url( '/' ),
			$path
		);

		return $file;
	}

	public function get_contents( $file ) {
		ob_start();
		include $this->get_file( $file );
		return ob_get_clean();
	}

	public function get_template( $file, $data = [] ) {
		extract( $data );
		ob_start();
		include $this->get_file( "templates/$file" );
		return ob_get_clean();
	}

	public function go() {
		$this->loader->run();
	}
}
