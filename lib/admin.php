<?php 

namespace IE_Form;

use WP_Roles;

if ( class_exists( 'IE' ) ) {
	return;
}

class Admin {

	public $roles = [
		'administrator',
		'editor',
		'author',
		'subscriber',
		'contributor'
	];

	public $new_roles = [
		'facilitator',
		'teacher',
		'student',
	];

	public function register_roles() {
		global $wp_roles;
		if ( ! $wp_roles) {
			$wp_roles = new WP_Roles();
		}

		$registered = get_option('ie-form-roles_' . filemtime(__FILE__));
		if ( $registered ) {
			return;
		}

		$wp_roles->remove_role('facilitator');
		$facilitator = $wp_roles->get_role('facilitator');
		if ( ! $facilitator ) {
			$facilitator = $wp_roles->add_role(
				'facilitator', 
				'Facilitator', [
					'create_users' => true,
					'delete_users' => true,
					'promote_users' => true,
					'edit_users' => true,
					'list_users' => true,
					'read' => true, // Allow access to admin screen.
			]);


		}

		$wp_roles->remove_role('teacher');
		$teacher = $wp_roles->get_role('teacher');
		if ( ! $teacher ) {
			$teacher = $wp_roles->add_role(
				'teacher', 
				'Teacher',
				[
					'read' => true, // Allow access to admin screen.
				]
			);
		}

		$wp_roles->remove_role('student');
		$student = $wp_roles->get_role('student');
		if ( ! $student ) {
			$student = $wp_roles->add_role(
				'student', 
				'Student',
				[
					'read' => true, // Allow access to admin screen.
				]
			);
		}

		update_option( 'ie-form-roles_' . filemtime(__FILE__), true );
	}

	public function admin_notices() {
		if ( ! $this->has_role('administrator') ) {
			remove_all_actions('admin_notices');
		}
	}

	public function admin_scripts() {
		wp_enqueue_style('ie-form-admin', IE_FORM_URL . '/dist/admin.css');
	}

	public function admin_menu() {
		global $menu;

		if ( ! $this->has_role('facilitator') ) {
			return;
		}

		foreach ($menu as $key => $menu_opt) {
			if ( $menu_opt[0] == 'Dashboard' ) {
				$menu[$key][1] = 'edit_posts';
			}

			if ( $menu_opt[0] == 'MemberPress' ) {
				unset( $menu[$key ]);
			}
		}
	}

	public function admin_bar_menu( \WP_Admin_Bar $admin_bar ) {

		if ( $this->has_role('administrator') ) {
			return;
		}

		$admin_bar->remove_node('wp-logo');
	}

	public function has_role( $role = '' ) {
		$user = wp_get_current_user();
		if ( ! $user ) {
			return false;
		}

		if ( in_array($role, $user->roles, true) ) {
			return true;
		}

		return false;
	}

	public function editable_roles( $roles ) {

		if ( ! $this->has_role('facilitator') ) {
			return $roles;
		}

		foreach ($this->roles as $role) {
			if ( isset( $roles[$role] ) ) {
				unset( $roles[$role]);
			}
		}

		return $roles;
	}

	public function admin_views_users($views) {

		if ( ! $this->has_role('facilitator') ) {
			return $views;
		}

		global $wp_roles;

		$all_count = 0;

		foreach ($views as $key => $view) {
			if ( in_array( $key, $this->roles, true ) ) {
				unset( $views[$key]);
				continue;
			}

			if ( $key == 'all' ) {
				continue;
			}

			preg_match( '/\((\d+)\)/', $view, $counts );
			if ( $counts ) {
				$all_count += (int) $counts[1];
			}
		}

		$views['all'] = preg_filter( '/\((\d+)\)/', "($all_count)", $views['all'], 1);

		return $views;
	}

	public function manage_users_columns( $columns ) {
		unset( $columns['posts' ] );
		unset( $columns['mepr_registered' ] );
		unset( $columns['mepr_last_login' ] );
		unset( $columns['mepr_num_logins' ] );

		$columns['teacher'] = __( 'Teacher', 'ie_form' );

		return $columns;
	}

	public function manage_users_sortable_columns( $columns ) {
		$columns['teacher'] = 'teacher';

		return $columns;
	}

	public function manage_users_custom_column( $value, $key, $user_id ) {

		if ( $key == 'teacher') {
			$teacher = get_field('teacher', "user_$user_id");
			if ( ! $teacher ) {
				return '-';
			} else {
				$t_user = get_user_by('id',$teacher);
				return $t_user->display_name;
			}
		}

		return $value;
	}

	public function pre_get_users( \WP_User_Query $query ) {
		global $pagenow;

		if ( ! is_admin() || $pagenow != 'users.php' ) {
			return;
		}

		// Order by teacher
		$orderby = $query->get('orderby');
		if ( $orderby && $orderby == 'teacher' ) {
			$meta_query = [
				'relation' => 'OR',
				[
					'key' => $orderby,
					'compare' => 'NOT EXISTS',
				],
				[
					'key' => $orderby
				],
			];
			$query->set('meta_query', $meta_query);
			$query->set('orderby', 'meta_value');
		}

		// Stuff for facilitators only.
		if (! $this->has_role('facilitator') ) {
			return;
		}

		$query->set('role__in', [
			'facilitator',
			'teacher',
			'student',
		]);
	}

}
