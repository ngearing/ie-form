<?php

namespace IE_Form;

use GFAPI;
use \setasign\Fpdi\Fpdi;

class IE_Entry {

	var $id = false;

	public function __construct( $id ) {
		$entry = GFAPI::get_entry( $id );
		if ( ! $entry ) {
			return false;
		}

		$this->entry = $entry;
		$this->id    = $id;
	}

	public function set_score( $score, $form_id ) {
		return gform_update_meta( $this->id, 'ie_scores', $score, $form_id );
	}

	public function update_meta( $key, $value ) {
		return gform_update_meta( $this->id, $key, $value );
	}

	public function get_score() {
		$scores = gform_get_meta( $this->id, 'ie_scores' );
		$order  = [
			'ERE' => 1,
			'UEO' => 1,
			'EDC' => 1,
			'EMC' => 1,
		];

		$scores = array_replace( $order, $scores );

		return $scores;
	}

	public function get_score_label( $key ) {
		$labels = [
			'ERE' => 'Emotional Recognition and Expression',
			'UEO' => 'Understanding the Emotions of Others',
			'EDC' => 'Emotional Reasoning',
			'EMC' => 'Emotional Management Control',
		];

		if ( isset( $labels[ $key ] ) ) {
			return $labels[ $key ];
		}

		return false;
	}

	private function get_score_page( $score ) {
		$brackets = [
			1 => 30,
			2 => 50,
			3 => 70,
		];

		$page = 1;
		foreach ( $brackets as $key => $value ) {
			if ( $score >= $value ) {
				$page = $key;
			}
		}

		return $page;
	}

	public function create_pdf() {

		$user_id = $this->entry['created_by'];
		$user    = get_user_by( 'id', $user_id );
		$user    = (object) [
			'id'   => $user_id,
			'name' => $user->user_firstname ? "$user->user_firstname $user->user_lastname" : 'David Tenent',
		];

		$scores  = $this->get_score();
		$hash    = hash( 'md5', json_encode( $scores ) );
		$uploads = wp_upload_dir()['basedir'] . '/ie-form';
		$ie      = new IE();

		// Create pdf file.
		$pdf = new Fpdi();
		// Set source pdf.
		$base_dir    = IE_FORM_PATH . '/src/templates/pdfs/';
		$base_file   = $base_dir . 'base.pdf';
		$page_count  = $pdf->setSourceFile( $base_file );
		$page_count += 4; // Add extra pages for the score pages.

		$score_templates = [
			12 => [
				'key'     => 'ERE',
				'page_no' => 12,
			],
			13 => [
				'key'     => 'UEO',
				'page_no' => 13,
			],
			14 => [
				'key'     => 'EDC',
				'page_no' => 14,
			],
			15 => [
				'key'     => 'EMC',
				'page_no' => 15,
			],
		];

		// Setup template files for pdf.
		$templates = [];
		for ( $page_no = 1; $page_no <= $page_count; $page_no++ ) {
			$templates[ $page_no ] = [
				'file'             => $base_file,
				'template_page_no' => $page_no > 11 ? $page_no - 4 : $page_no,
			];

			// Set the templates for the score pages.
			if ( $page_no > 11 & $page_no < 16 ) {
				$score_key  = $score_templates[ $page_no ]['key'];
				$score_page = $this->get_score_page( $scores[ $score_key ]['percent'] );

				$templates[ $page_no ] = [
					'file'             => "$base_dir/score-$score_key-$score_page.pdf",
					'template_page_no' => 1, // There is only 1 page for the score templates.
				];
			}
		}

		// Loop through templates and create pdf pages.
		for ( $page_no = 1; $page_no <= $page_count; $page_no++ ) {

			// Add page from source pdf.
			$template_page = $templates[ $page_no ]['template_page_no'];
			$template_file = $templates[ $page_no ]['file'];

			$pdf->setSourceFile( $template_file );
			$tpl_page = $pdf->importPage( $template_page );
			$tpl_size = $pdf->getTemplateSize( $tpl_page );
			$pdf->AddPage( $tpl_size['orientation'], $tpl_size );
			$pdf->useImportedPage( $tpl_page, [ 'adjustPageSize' => true ] );

			// Add text to the page.
			if ( $page_no == 1 ) {
				$pdf->AddFont( 'NexaHeavy', '', 'NexaHeavy-Regular.php' );
				$pdf->SetFont( 'NexaHeavy', '', 22 );
				$pdf->SetTextColor( 53, 73, 90 );
				$pdf->SetXY( 55, 150, true );
				$pdf->Cell( 100, 6, $user->name, 0, 0, 'C' );
			}

			if ( $page_no == 2 ) {
				$pdf->AddFont( 'RobotoSlab', '', 'RobotoSlab-Light.php' );
				$pdf->SetFont( 'RobotoSlab', '', 12 );
				$pdf->SetTextColor( 35, 31, 32 );
				$pdf->SetXY( 38, 66, true );
				$pdf->Cell( 60, 5, $user->name );
				$pdf->SetXY( 38, 74, true );
				$pdf->Cell( 60, 5, date( 'F jS Y' ) );
			}
		}

		$file = "$uploads/$user_id-results.pdf";

		$pdf->Output( $file, 'F' );

		return $file;
	}

	public function get_results_url() {
		$user_id = $this->entry['created_by'];
		$uploads = wp_upload_dir()['basedir'] . '/ie-form';
		$file    = "$uploads/$user_id-results.pdf";
		return ( new IE() )->path_to_url( $file );
	}
}
