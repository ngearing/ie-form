<?php

namespace IE_Form;

use GFAPI;

if ( class_exists( 'IE_Form_Dev' ) ) {
	return;
}

class IE_Form_Dev extends IE_Form {

	public function render_test_submit( $form_string, $form ) {

		if ( $form['id'] == $this->id ) {
			$form_string = sprintf(
				'%s<button type="submit" value="on" name="test-submit">Test form submit</button>',
				$form_string
			);
		}

		return $form_string;
	}

	public function pre_val( $form ) {
		if ( rgpost( 'test-submit' ) != 'on' ) {
			return $form;
		}
		$fields = $form['fields'];

		foreach ( $fields as $key => $field ) {
			if ( is_a( $field, 'GF_Field_Page' ) ) {
				continue;
			}

			if ( $field->isRequired ) {
				$field->isRequired = false;
			}
		}

		$form['fields'] = $fields;

		return $form;
	}

	public function pre_pro( $form ) {
		if ( rgpost( 'test-submit' ) != 'on' ) {
			return $form;
		}

		$values = [];
		foreach ( $form['fields'] as $key => $field ) {
			if ( is_a( $field, 'GF_Field_Page' ) ) {
				continue;
			}

			$value = rgpost( 'input_' . $field->id );
			if ( ! $value ) {
				$value = rand( 1, 5 );
			}

			$values[ 'input_' . $field->id ] = $value;
		}

		// Empty post so it does not cause any issues with test submit.
		$_POST = null;

		$result = GFAPI::submit_form( $form['id'], $values, false );
		// $form = array_replace( $form, $result );

		return false;
	}

	public function set_rand_values( $form ) {

		$_POST['input_11'] = '1';

		return $form;
	}
}
