<?php

namespace IE_Form;

class Redirect 
{


	public function template_redirect() {}
	public function wp_login($user_login, \WP_User $user) {

		if ( in_array('facilitator', $user->roles, true ) ) {
			wp_safe_redirect( admin_url('users.php') );
			die;
		}
	}
}
