# PHP Code Sniffing for WordPress

## Requirements

Composer

## Install

```bash
composer update
```

## How to use

From the root of your project directory run the following from your terminal:

```bash
vendor/bin/phpcs .
```

Or to fix errors

```bash
vendor/bin/phpcbf .
```
