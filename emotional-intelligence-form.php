<?php
/**
 * Plugin Name: Emotional Intelligence Form
 */

if ( ! ABSPATH ) {
	wp_die();
}

define( 'IE_FORM_PATH', __DIR__ );
define( 'IE_FORM_URL', plugins_url( '', __FILE__ ) );
define( 'FPDF_FONTPATH', IE_FORM_PATH . '/src/fonts/' );

register_activation_hook(
	__FILE__,
	function() {
		if ( ! class_exists( 'IE_Form\IE_Form' ) ) {
			require __DIR__ . '/lib/ie-form.php';
		}

		\IE_Form\IE_Form::create();
	}
);

function start_ie_form() {
	require __DIR__ . '/lib/ie.php';

	$plugin = new \IE_Form\IE();
	$plugin->go();
}
start_ie_form();
